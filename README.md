# pss_ass3

This project requires java-1.7-oracle.
This project requires the library EclipseLink 2.5+.
Please make sure that this library is correctly installed on your system and linked correctly to the project.
The testcases implemented for this project exploit Junit, so please be sure that you have installed a version of this library grater than 4.11.

To use this package, simply import the provided entities and the related handlers and services in your current project.
A set of tests for both handlers and services is provided, please incorporate them with tests of your current project.

If you just want to test the functionality implemented, please run the test packeges (ServiceTests and HandlerTests) with Juint.

The project exploit a private MySQL server istance, as specified in the persistence.xml file; you can specify your own instance of any MySQL server by modifying the persistence.xml.