package exceptions;

public class TupleNotFoundException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public TupleNotFoundException() {
		super("The tuple was not found");
	}

	public TupleNotFoundException(String message) {
		super(message);
	}

}
