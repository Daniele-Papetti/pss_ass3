package entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
@Table
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public class Veicolo implements Serializable {
	
	private static final long serialVersionUID = 42L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String targa;
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn
	private CasaProduttrice marca;
	
	// Constructors
	
	public Veicolo() {
		super();
	}

	public Veicolo(String targa, CasaProduttrice cp) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setTarga(targa);
		this.setMarca(cp);
	}

	// Getters and Setters
	
	public String getTarga() {
		return targa;
	}

	protected void setTarga(String targa) throws NotValidPrimaryKeyExcpetion {
		if(targa.length() == 7)
			this.targa = targa;
		else
			throw new NotValidPrimaryKeyExcpetion("La targa deve essere di 7 caratteri!");
	}
	
	public CasaProduttrice getMarca() {
		if(this.marca == null)
			return null;
		try {
			return new CasaProduttrice(this.marca);
		}
		catch (NotValidPrimaryKeyExcpetion e) {
			return null;
		}
	}

	public void setMarca(CasaProduttrice cp) {
		if(cp == null)
			this.marca = null;
		else {
			try {
				this.marca = new CasaProduttrice(cp);
			} catch (NotValidPrimaryKeyExcpetion e) {
				this.marca = null;
			}
		}
	}

	// Other methods
	
	@Override
	public String toString() {
		return "Targa: " + targa + "\ncasa produttrice: " + marca.getNome();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veicolo other = (Veicolo) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (targa == null) {
			if (other.targa != null)
				return false;
		} else if (!targa.equals(other.targa))
			return false;
		return true;
	}	
}