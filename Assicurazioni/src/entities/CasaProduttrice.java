package entities;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.eclipse.persistence.indirection.IndirectSet;

import javax.persistence.FetchType;

import exceptions.NotValidPrimaryKeyExcpetion;

import java.util.Set;

@Entity
@Table
public class CasaProduttrice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String nome;
	@Column(nullable = false)
	private String sede_legale;
	private String referente;
	
	@ManyToMany(targetEntity=Meccanico.class, fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name="CP_MEC", joinColumns = @JoinColumn(name="CP_ID"), inverseJoinColumns = @JoinColumn(name="MEC_ID"))
	private Set<Meccanico> meccanicoSet;
	
	// Constructors
	
	public CasaProduttrice() {
		super();
		this.meccanicoSet = new IndirectSet();		
	}
	
	public CasaProduttrice(String nome, String sede_legale, String referente, Set<Meccanico> meccanicoSet) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setNome(nome);
		this.setSede_legale(sede_legale);
		this.setReferente(referente);
		if (meccanicoSet == null)
			this.meccanicoSet = new IndirectSet();
		else
			this.setMeccanicoSet(meccanicoSet);
	}

	public CasaProduttrice(CasaProduttrice cp) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(cp);
	}
	
	// Getters and Setters
	
	public String getNome() {
		return nome;
	}
	
	private void setNome(String nome) throws NotValidPrimaryKeyExcpetion {
		if(nome.equals(""))
			throw new NotValidPrimaryKeyExcpetion("Il nome di una socità non può essere nullo");
		else
			this.nome = nome;
	}

	public String getSede_legale() {
		return sede_legale;
	}
	
	public void setSede_legale(String sede_legale) {
		this.sede_legale = sede_legale;
	}
	
	public String getReferente() {
		return referente;
	}
	
	public void setReferente(String referente) {
		this.referente = referente;
	}
	
	public Set<Meccanico> getMeccanicoSet() {
		if(this.meccanicoSet == null)
			return new IndirectSet();
		try {
			return new IndirectSet(this.meccanicoSet);
		} catch (NullPointerException e) {
			return new IndirectSet();
		}
	}

	public void setMeccanicoSet(Set<Meccanico> meccanicoSet) {
		if(meccanicoSet == null)
			this.meccanicoSet = new IndirectSet();
		else {
			try {
				this.meccanicoSet = new IndirectSet(meccanicoSet);
			} catch (NullPointerException e) {
				this.meccanicoSet = new IndirectSet();
			}
		}
	}
	
	// Other methods
	
	@Override
	public String toString() {
		return "Nome: " + nome + "\nsede_legale: " + sede_legale + "\nreferente: " + referente + "\nmeccanicoSet:\n\t" + meccanicoSet;
	}

	public void setEqual (CasaProduttrice casa_produttrice) throws NotValidPrimaryKeyExcpetion {
		this.setNome(casa_produttrice.nome);
		this.setSede_legale(casa_produttrice.sede_legale);
		this.setReferente(casa_produttrice.referente);
		this.setMeccanicoSet(casa_produttrice.meccanicoSet);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CasaProduttrice other = (CasaProduttrice) obj;
		boolean thisFlag = true;
		boolean otherFlag = false;
		if (meccanicoSet == null || meccanicoSet.isEmpty()) {
			if (other.meccanicoSet != null || !meccanicoSet.isEmpty())
				thisFlag = false;
		} else 
		{ 
			if (meccanicoSet.size() != other.meccanicoSet.size())
				return false;
			else{
				for (Meccanico m : meccanicoSet) {
			    	   for (Meccanico om : other.meccanicoSet) {
			    		   if( m.getNome().equals(om.getNome()))
			    			   otherFlag = true;
			    	   }
			    	   thisFlag = otherFlag && thisFlag;
			    	   otherFlag = false;
			    	}
			}
		}
		if ((meccanicoSet == null || meccanicoSet.isEmpty()) || (other.meccanicoSet == null || other.meccanicoSet.isEmpty()) )
			thisFlag = true;
		if (!thisFlag)
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (referente == null) {
			if (other.referente != null)
				return false;
		} else if (!referente.equals(other.referente))
			return false;
		if (sede_legale == null) {
			if (other.sede_legale != null)
				return false;
		} else if (!sede_legale.equals(other.sede_legale))
			return false;
		return true;
	}	
}
