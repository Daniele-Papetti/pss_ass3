package entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
@Table
public class Meccanico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String piva;
	@Column(nullable = false)
	private String nome;
	private String citta;
	private String via;
	private String num_tel;
	
	// Constructors
	
	public Meccanico() {
		super();
	}

	public Meccanico(String piva, String nome, String citta, String via, String num_tel) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setPiva(piva);
		this.setNome(nome);
		this.setCitta(citta);
		this.setVia(via);
		this.setNum_tel(num_tel);
	}

	public Meccanico(Meccanico meccanico) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(meccanico);
	}
	
	// Getters and Setters
	
	public String getPiva() {
		return piva;
	}

	private void setPiva(String piva) throws NotValidPrimaryKeyExcpetion {
		if(piva.length() == 11)
			this.piva = piva;
		else
			throw new NotValidPrimaryKeyExcpetion("La partita iva deve essere di 11 numeri!");
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getNum_tel() {
		return num_tel;
	}

	public void setNum_tel(String num_tel) {
		this.num_tel = num_tel;
	}
	
	// Other methods
	
	@Override
	public String toString() {
		return "Partita IVA: " + piva + "\nnome: " + nome + "\ncitta: " + citta + "\nvia: " + via + "\nnum_tel: "
				+ num_tel;
	}

	public void setEqual(Meccanico meccanico) throws NotValidPrimaryKeyExcpetion {
		this.setPiva(meccanico.piva);
		this.setNome(meccanico.nome);
		this.setCitta(meccanico.citta);
		this.setVia(meccanico.via);
		this.setNum_tel(meccanico.num_tel);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Meccanico other = (Meccanico) obj;
		if (citta == null) {
			if (other.citta != null)
				return false;
		} else if (!citta.equals(other.citta))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (num_tel == null) {
			if (other.num_tel != null)
				return false;
		} else if (!num_tel.equals(other.num_tel))
			return false;
		if (piva == null) {
			if (other.piva != null)
				return false;
		} else if (!piva.equals(other.piva))
			return false;
		if (via == null) {
			if (other.via != null)
				return false;
		} else if (!via.equals(other.via))
			return false;
		return true;
	}
}
