package entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
public class Auto extends Veicolo {
	
	private static final long serialVersionUID = 43L;
	
	@Column(nullable = false)
	private String modello;
	private short cilindrata;
	private int valore;
	private int km;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn
	private Guidatore guidatore;
	
	// Constructors
	
	public Auto() {
		super();
	}

	public Auto(String targa, CasaProduttrice cp, String modello, short cilindrata, int valore, int km,
			Guidatore guidatore) throws NotValidPrimaryKeyExcpetion {
		super(targa, cp);
		this.setModello(modello);
		this.setCilindrata(cilindrata);
		this.setValore(valore);
		this.setKm(km);
		this.setGuidatore(guidatore);
	}

	public Auto(Auto auto) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(auto);
	}
	
	// Getters and Setters
	
	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public short getCilindrata() {
		return cilindrata;
	}

	public void setCilindrata(short cilindrata) {
		this.cilindrata = cilindrata;
	}

	public int getValore() {
		return valore;
	}

	public void setValore(int valore) {
		this.valore = valore;
	}

	public int getKm() {
		return km;
	}

	public void setKm(int km) {
		this.km = km;
	}

	public Guidatore getGuidatore() {
		if(this.guidatore == null) {
			return null;
		}
		try {
			return new Guidatore(this.guidatore);
		} catch (NotValidPrimaryKeyExcpetion e) {
			return null;
		}
	}

	public void setGuidatore(Guidatore guidatore) {
		if(guidatore == null)
			this.guidatore = null;
		else {
			try {
				this.guidatore = new Guidatore(guidatore);
			} catch (NotValidPrimaryKeyExcpetion e) {
				this.guidatore = null;
			}
		}
	}

	// Other methods
	
	@Override
	public String toString() {
		return super.toString() + "\nmodello: " + modello + "\ncilindrata: " + cilindrata + "\nvalore: " + valore + "\nKm: " + km
				+ "\nguidatore:\n\t" + guidatore;
	}
	
	public void setEqual(Auto auto) throws NotValidPrimaryKeyExcpetion {
		this.setTarga(auto.getTarga());
		this.setMarca(auto.getMarca());
		this.setModello(auto.modello);
		this.setCilindrata(auto.cilindrata);
		this.setValore(auto.valore);
		this.setKm(auto.km);
		this.setGuidatore(auto.guidatore);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Auto other = (Auto) obj;
		if (cilindrata != other.cilindrata)
			return false;
		if (guidatore == null) {
			if (other.guidatore != null)
				return false;
		} else if (!guidatore.equals(other.guidatore))
			return false;
		if (km != other.km)
			return false;
		if (modello == null) {
			if (other.modello != null)
				return false;
		} else if (!modello.equals(other.modello))
			return false;
		if (valore != other.valore)
			return false;
		return true;
	}
}
