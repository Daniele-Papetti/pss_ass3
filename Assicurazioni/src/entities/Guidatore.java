package entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import exceptions.NotValidPrimaryKeyExcpetion;

@Entity
@Table
public class Guidatore implements Serializable{
	
	private static final long serialVersionUID = 45L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String CF;
	@Column(nullable = false)
	private String nome;
	@Column(nullable = false)
	private String cognome;
	private String data_di_nascita;
	private String classe;
	private String citta;
	
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn
	private Guidatore genitore;
	
	// Constructors
	
	public Guidatore() {
		super();
	}
	
	public Guidatore(String cF, String nome, String cognome, String data_di_nascita, String classe, String citta, Guidatore genitore) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setCF(cF);
		this.setNome(nome);
		this.setCognome(cognome);
		this.setData_di_nascita(data_di_nascita);
		this.setClasse(classe);
		this.setCitta(citta);
		this.setGenitore(genitore);
	}

	public Guidatore(Guidatore guidatore) throws NotValidPrimaryKeyExcpetion {
		super();
		this.setEqual(guidatore);
	}
	
	// Getters and Setters
	
	public String getCF() {
		return CF;
	}
	
	private void setCF(String cF) throws NotValidPrimaryKeyExcpetion {
		if(cF.length() == 16)
			this.CF = cF;
		else
			throw new NotValidPrimaryKeyExcpetion("Il codice fiscale deve essere di 16 caratteri!");
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCognome() {
		return cognome;
	}
	
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	public String getData_di_nascita() {
		return data_di_nascita;
	}
	
	public void setData_di_nascita(String data_di_nascita) {
		this.data_di_nascita = data_di_nascita;
	}
	
	public String getClasse() {
		return classe;
	}
	
	public void setClasse(String classe) {
		this.classe = classe;
	}
	
	public String getCitta() {
		return citta;
	}
	
	public void setCitta(String citta) {
		this.citta = citta;
	}

	public Guidatore getGenitore() {
		if(this.genitore == null)
			return null;
		try {
			return new Guidatore(this.genitore);
		} catch (NotValidPrimaryKeyExcpetion e) {
			return null;
		}
	}

	public void setGenitore(Guidatore genitore) {
		if(genitore == null)
			this.genitore = null;
		else {
			try {
				this.genitore = new Guidatore(genitore);
			} catch (NotValidPrimaryKeyExcpetion e) {
				this.genitore = null;
			}
		}
	}
	
	
	public void setEqual (Guidatore guidatore) throws NotValidPrimaryKeyExcpetion {
		this.setCF(guidatore.CF);
		this.setNome(guidatore.nome);
		this.setCognome(guidatore.cognome);
		this.setData_di_nascita(guidatore.data_di_nascita);
		this.setClasse(guidatore.classe);
		this.setCitta(guidatore.citta);
		this.setGenitore(guidatore.genitore);
	}
	
	// Other methods
	
	@Override
	public String toString() {
		return "Codice Fiscale: " + CF + "\nnome: " + nome + "\ncognome: " + cognome + "\ndata_di_nascita: "
				+ data_di_nascita + "\nclasse: " + classe + "\ncitta: " + citta + "\ngenitore:\n\t" + genitore;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Guidatore other = (Guidatore) obj;
		if (CF == null) {
			if (other.CF != null)
				return false;
		} else if (!CF.equals(other.CF))
			return false;
		if (citta == null) {
			if (other.citta != null)
				return false;
		} else if (!citta.equals(other.citta))
			return false;
		if (classe == null) {
			if (other.classe != null)
				return false;
		} else if (!classe.equals(other.classe))
			return false;
		if (cognome == null) {
			if (other.cognome != null)
				return false;
		} else if (!cognome.equals(other.cognome))
			return false;
		if (data_di_nascita == null) {
			if (other.data_di_nascita != null)
				return false;
		} else if (!data_di_nascita.equals(other.data_di_nascita))
			return false;
		if (genitore == null) {
			if (other.genitore != null)
				return false;
		} else if (!genitore.equals(other.genitore))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
}
