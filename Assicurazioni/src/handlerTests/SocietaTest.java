package handlerTests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import exceptions.TupleNotFoundException;
import entities.Bilico;
import entities.Societa;
import handlers.BilicoHandler;
import handlers.SocietaHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SocietaTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Societa> utilizedSocieta;
	private static Set<Bilico> utilizedBilici;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedSocieta = new HashSet<Societa>();
		utilizedBilici = new HashSet<Bilico>();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Bilico bilico : utilizedBilici) {
			try {
				BilicoHandler.deleteTuple(bilico.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
		for (Societa societa : utilizedSocieta) {
			try {
				SocietaHandler.deleteTuple(societa.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}

	@Test
	public void stage1_SuccesfullAddTupleAndFindTuple() {
		Societa s = null;
		try {
			s = new Societa("13395630821", "The real big three", "Boston", "Winning championships");
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    SocietaHandler.addTuple(s, entitymanager);

	    Societa found = null;
	    Societa found2 = null;
	    try{
	    	found = SocietaHandler.findTuple("13395630821", entitymanager);
	    	found2 = SocietaHandler.findTuple("13395630821", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(s.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2);
	}
	
	@Test
	public void stage2_SuccesfullUpdateTuple() {
		Societa s = null;
		try {
			s = SocietaHandler.findTuple("13395630821", entitymanager);
			utilizedSocieta.add(s);
		}
		catch(Exception e){
			fail(e.getMessage());
		}
		s.setNome("In Brad Stevens we belive");
		s.setRagione_sociale("Test");
		try {
			SocietaHandler.updateTuple(s, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		
		Societa found = null;
		try {
			found = SocietaHandler.findTuple("13395630821", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(s.equals(found));
	}
	
	@Test
	public void stage3_SuccesfullDeleteTuple() {
	    try {
	    	SocietaHandler.deleteTuple("13395630821", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	SocietaHandler.findTuple("13395630821", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fail(); // this is executed only if it finds a tuple which is not suppose to be there
	}
	
	@Test
	public void stage4_FailFindTuple() {
		try {
			SocietaHandler.findTuple("99999999999", entitymanager);
		}	    
		catch(TupleNotFoundException e) {
	    	return;
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage5_FailAddTuple() {
		Societa s = null;
		Societa s2 = null;
		try {
			s = new Societa("13395630821", "The real big three", "Boston", "Winning championships");
			utilizedSocieta.add(s);
			s2 = new Societa("13395630821", "The splash Brothers", "San Francisco", "3pt");
			utilizedSocieta.add(s2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    SocietaHandler.addTuple(s, entitymanager);
	    try {
	    	SocietaHandler.addTuple(s2, entitymanager);
	    }
	    catch(EntityExistsException e) {
	    	return;
	    }
	    catch(PersistenceException e) { 
	    	return;	    	
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fail();
	}
	
	@Test
	public void stage6_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		Societa s = null;
		try {
			s = new Societa("99999999999", "Superman is in the building", "Los Angeles", "No real reason");
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			SocietaHandler.updateTuple(s, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage7_FailDeleteTuple() {
		try {
			SocietaHandler.deleteTuple("99999999999", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage8_RemoveSocietaReferencedToBilico() {
		// For this test we suppose that Bilico and its handler works fine and they do not have 
		// problems of any kind.
		Bilico b = null;
		Societa s = null;
		try {
			s = new Societa("12312312319", "TestSocieta", "Milano", "Test");
			utilizedSocieta.add(s);
			b = new Bilico("ZZ000ZZ", null, 30, 31, (float) 2.1, (float) 3.1, s);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    BilicoHandler.addTuple(b, entitymanager);
	
	    try {
	    	SocietaHandler.deleteTuple("12312312319", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	SocietaHandler.findTuple("12312312319", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we want
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that b is still in the db
	    Bilico found = null;
	    try {
	    	found = BilicoHandler.findTuple("ZZ000ZZ", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
		}
	    assert(found.getSocieta() == null);
	}	
}