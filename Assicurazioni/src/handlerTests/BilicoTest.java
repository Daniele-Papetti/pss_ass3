package handlerTests;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Bilico;
import entities.CasaProduttrice;
import entities.Societa;
import exceptions.TupleNotFoundException;
import handlers.BilicoHandler;
import handlers.CasaProduttriceHandler;
import handlers.SocietaHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class BilicoTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Bilico> utilizedBilici;
	private static Set<Societa> utilizedSocieta;
	private static Set<CasaProduttrice> utilizedCaseProduttrici;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedBilici = new HashSet<Bilico>();
		utilizedSocieta = new HashSet<Societa>();
		utilizedCaseProduttrici = new HashSet<CasaProduttrice>();
	}
	
	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Bilico bilico : utilizedBilici) {
			try {
				BilicoHandler.deleteTuple(bilico.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
		for (Societa societa : utilizedSocieta) {
			try {
				SocietaHandler.deleteTuple(societa.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice cp : utilizedCaseProduttrici) {
			try {
				CasaProduttriceHandler.deleteTuple(cp.getNome(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullAddTupleAndFindTupleNull() {
		Bilico b = null;
		try {
			b = new Bilico("AA000AA", null, 42, 42, (float) 42.5, (float) 3.4, null);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    BilicoHandler.addTuple(b, entitymanager);
	    
	    Bilico found = null;
	    Bilico found2 = null;
	    try {
	    	found = BilicoHandler.findTuple("AA000AA", entitymanager);
	    	found2 = BilicoHandler.findTuple("AA000AA", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2); 
	}

	@Test
	public void stage02_SuccesfullUpdateTupleNull() {
	    Bilico b = null;
	    try{
	    	b = BilicoHandler.findTuple("AA000AA", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    b.setAltezza((float) 4.1);
	    try {
	    	BilicoHandler.updateTuple(b, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Bilico found = null;
	    try {
	    	found = BilicoHandler.findTuple("AA000AA", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullDeleteTupleNull() {
		try {
			BilicoHandler.deleteTuple("AA000AA", entitymanager);
		}
		catch(Exception e) {
	    	fail(e.getMessage());
	    }
		try {
			BilicoHandler.findTuple("AA000AA", entitymanager);
		}
		catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		fail(); // this is executed only if it finds a tuple which is not suppose to be there
	}
	
	@Test
	public void stage04_SuccesfullAddTupleAndFindTuple() {
		Bilico b = null;
		CasaProduttrice cp = null;
		Societa s = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttrice", "Roma", "Francesco Sforza", null);
			utilizedCaseProduttrici.add(cp);
			s = new Societa("12312312312", "TestSocieta", "Milano", "Test");
			utilizedSocieta.add(s);
			b = new Bilico("BB000BB", cp, 30, 31, (float) 2.1, (float) 3.1, s);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    BilicoHandler.addTuple(b, entitymanager);
	    
	    Bilico found = null;
	    Bilico found2 = null;
	    try {
	    	found = BilicoHandler.findTuple("BB000BB", entitymanager);
	    	found2 = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	    assert(found == found2);
	    
	    // We also check if s and cp are in DB, we do not control if we search them twice the object 
	    // is loaded only once; this is tested in their tests.
	    Societa sFound = null;
	    try {
	    	sFound = SocietaHandler.findTuple("12312312312", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(s.equals(sFound));
	    
	    CasaProduttrice cpFound = null;
	    try {
	    	cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttrice", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(cp.equals(cpFound));
	}

	@Test
	public void stage05_SuccesfullAddTupleAlreadyExistingExternalKey() {
		Bilico b = null;
		CasaProduttrice cp = null;
		Societa s = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova", "Pordenone", "Mario Rossi", null); 
			// In this way we test both the case of a not already existing external key and the 
			// already existing external key
			utilizedCaseProduttrici.add(cp);
			s = new Societa("12312312312", "TestSocieta", "Milano", "Test");
			utilizedSocieta.add(s);
			b = new Bilico("CC000CC", cp, 30, 31, (float) 2.1, (float) 3.1, s);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    BilicoHandler.addTuple(b, entitymanager);
	    
	    Bilico found = null;
	    Bilico found2 = null;
	    try {
	    	found = BilicoHandler.findTuple("CC000CC", entitymanager);
	    	found2 = BilicoHandler.findTuple("CC000CC", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	    assert(found == found2);
	    // we test if the new CasaProduttrice is in the db
	    CasaProduttrice cpFound = null;
	    try {
	    	cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(cp.equals(cpFound));
	}
	
	@Test
	public void stage06_SuccesfullUpdateTupleNotExternalKey() {
	    Bilico b = null;
	    try {
	    	b = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
			fail(e.getMessage());
	    }
	    b.setAltezza((float) 2.95);
	    try {
	    	BilicoHandler.updateTuple(b, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Bilico found = null;
	    try {
	    	found = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	}
	
	@Test
	public void stage07_SuccesfullUpdateTupleExternalKeyNewFathers() {
		Bilico b = null;
		CasaProduttrice cp = null;
		Societa s = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova2", "Caglieri", "Massimo Bianco", null); 
			utilizedCaseProduttrici.add(cp);
			s = new Societa("12312312314", "TestSocieta", "Milano", "Test");
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    try {
	    	b = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
			fail(e.getMessage());
	    }
	    b.setSocieta(s);
	    b.setMarca(cp);
	    
	    try {
	    	BilicoHandler.updateTuple(b, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    
	    Bilico found = null;
	    try {
	    	found = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(found));
	    
	    // We also check if s and cp are in DB, we do not control if we search them twice the object 
	    // is loaded only once; this is tested in their tests. 
	    Societa sFound = null;
	    try {
	    	sFound = SocietaHandler.findTuple("12312312314", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(s.equals(sFound));
	    
	    CasaProduttrice cpFound = null;
	    try {
	    	cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova2", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(cp.equals(cpFound));
	}
	
	@Test
	public void stage08_SuccesfullUpdateTupleExternalKeyAlreadyExistingFather() {
		Societa s = null;
		CasaProduttrice cp = null;
		Bilico b = null;
		try {
			s = SocietaHandler.findTuple("12312312312", entitymanager);
			cp = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova", entitymanager);
			b = BilicoHandler.findTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		b.setSocieta(s);
		b.setMarca(cp);
	    try {
	    	BilicoHandler.updateTuple(b, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    Bilico bFound = null;
	    try {
	    	bFound = BilicoHandler.findTuple("BB000BB", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(b.equals(bFound));
	}
	
	@Test
	public void stage09_SuccesfullDeleteTuple() {
		try {
			BilicoHandler.deleteTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		// verifying that tuple does not exits anymore
		try {
			BilicoHandler.findTuple("BB000BB", entitymanager);
			fail(); // this is executed only if it finds a tuple which is not suppose to be there
		}
		catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // Checking if the fathers are still in the DB
		try {
			SocietaHandler.findTuple("12312312312", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage10_FailFindTuple() {
		try {
			BilicoHandler.findTuple("WW000WW", entitymanager);
		}
		catch(TupleNotFoundException e) {
	    	return;
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage11_FailAddTuple() {
		Societa s = null;
		CasaProduttrice cp = null;
		Bilico b = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova3", "Sirolo", "Marco Rossi", null); 
			utilizedCaseProduttrici.add(cp);
			s = new Societa("12312312399", "TestSocietaNuova", "Brescia", "Test");
			utilizedSocieta.add(s);
			b = new Bilico("CC000CC", cp, 30, 31, (float) 2.1, (float) 3.1, s);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			BilicoHandler.addTuple(b, entitymanager);
		}
		catch(EntityExistsException e) {
		   	return;
		}
		catch(Exception e) {
		   	fail(e.getMessage());
		}
		fail();
	}

	@Test
	public void stage12_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		Bilico b = null;
		try {
			b = new Bilico("WW000WW", null, 9, 1, (float) 1.97, (float) 2.1, null);
			utilizedBilici.add(b);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			BilicoHandler.updateTuple(b, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage13_FailDeleteTuple() {
		try {
			BilicoHandler.deleteTuple("OO000OO", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
}