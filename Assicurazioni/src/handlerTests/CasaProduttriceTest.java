package handlerTests;

import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnitUtil;

import org.eclipse.persistence.indirection.IndirectSet;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.FixMethodOrder;

import entities.CasaProduttrice;
import entities.Meccanico;
import exceptions.TupleNotFoundException;
import handlers.CasaProduttriceHandler;
import handlers.MeccanicoHandler;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CasaProduttriceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Meccanico> utilizedMeccanico;
	private static Set<CasaProduttrice> utilizedCasa;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedMeccanico = new HashSet<Meccanico>();
		utilizedCasa = new HashSet<CasaProduttrice>();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Meccanico m : utilizedMeccanico) {
			try {
				MeccanicoHandler.deleteTuple(m.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice c : utilizedCasa) {
			try {
				CasaProduttriceHandler.deleteTuple(c.getNome(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullAddTupleAndFindTupleNull() {
		CasaProduttrice fiat = null;
		try {
			fiat = new CasaProduttrice("Fiat", "Italia", "Anastasio", null);
		    utilizedCasa.add(fiat);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    CasaProduttriceHandler.addTuple(fiat, entitymanager);
	    
	    CasaProduttrice found = null;
	    CasaProduttrice found2 = null;
	    try{
	    	found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    	found2 = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(fiat.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2); 
	}
	
	@Test
	public void stage02_UpdateTupleNull() {
		CasaProduttrice fiat = null;
	    try{
	    	fiat = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fiat.setReferente("Antonio");
	    try{
	    	CasaProduttriceHandler.updateTuple(fiat, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(fiat.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullDeleteTupleNull() {
	    try {
	    	CasaProduttriceHandler.deleteTuple("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits
	    try {
	    	CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    }
	    catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    fail();
	}    

	
	@Test
	public void stage04_SuccesfullAddTupleAndFindTupleNotExistingMeccanici() {
		Meccanico luca = null;
		Meccanico mario = null;
		CasaProduttrice fiat = null;
		Set<Meccanico> mecSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  mario = new Meccanico("11001100110", "Mario", "Milano", "Via angelo", "3334564432");
	    	  utilizedMeccanico.add(mario);
	    	  fiat = new CasaProduttrice("Fiat", "Italia", "Anastasio", null);
	    	  utilizedCasa.add(fiat);
	    	  mecSet = new IndirectSet();
	    	  mecSet.add(luca);
	    	  mecSet.add(mario);
	    	  fiat.setMeccanicoSet(mecSet);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    CasaProduttriceHandler.addTuple(fiat, entitymanager);
	    
	    // checking if Fiat is in the DB
	    CasaProduttrice found = null;
	    CasaProduttrice found2 = null;
	    try{
	    	found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    	found2 = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(fiat.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2); 
	    
	    // checking if Luca and Mario are in the DB
	    try {
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    	MeccanicoHandler.findTuple("11001100110", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	}
	
	@Test
	public void stage05_SuccesfullAddTupleAlreadyExistingMeccanico() {
		Meccanico luca = null;
		Meccanico mario = null;
		CasaProduttrice nissan = null;
		Set<Meccanico> mecSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  mario = new Meccanico("11001100110", "Mario", "Milano", "Via angelo", "3334564432");
	    	  utilizedMeccanico.add(mario);
	    	  nissan = new CasaProduttrice("Nissan", "Italia", "Anastasio", null);
	    	  utilizedCasa.add(nissan);
	    	  mecSet = new IndirectSet();
	    	  mecSet.add(luca);
	    	  mecSet.add(mario);
	    	  nissan.setMeccanicoSet(mecSet);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    CasaProduttriceHandler.addTuple(nissan, entitymanager);
	    
	    // checking if Fiat is in the DB
	    CasaProduttrice found = null;
	    CasaProduttrice found2 = null;
	    try{
	    	found = CasaProduttriceHandler.findTuple("Nissan", entitymanager);
	    	found2 = CasaProduttriceHandler.findTuple("Nissan", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(nissan.equals(found));
	    // We are checking that the same object is not loaded twice
	    assert(found == found2); 
	}
	
	
	@Test
	public void stage06_LazyLoadingMeccanicoSet() {
		// Closing the old EM and opening a new one is required, otherwise it will exploit the cached entity and it will not make any SQL query 
		entitymanager.close();
    	emfactory.close();
    	emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		PersistenceUnitUtil unitUtil = entitymanager.getEntityManagerFactory().getPersistenceUnitUtil();
		CasaProduttrice found = null;
		try {
			found = CasaProduttriceHandler.findTuple("Nissan", entitymanager);
		}	    
		catch(Exception e) {
			fail(e.getMessage());
		}
		// checking if the entity is loaded
		Assert.assertTrue(unitUtil.isLoaded(found));
		// checking if the field is loaded. I expect not to be loaded
		Assert.assertFalse(unitUtil.isLoaded(found, "meccanicoSet"));
		for (Meccanico m : found.getMeccanicoSet()) {
			m.getNome();
		}
		// checking if the field is loaded. I expect it to be loaded now
		Assert.assertTrue(unitUtil.isLoaded(found, "meccanicoSet"));
	}
	
	
	@Test
	public void stage07_SuccesfullUpdateTupleNotInvolvingMeccanico() {
		CasaProduttrice fiat = null;
		try {
			fiat = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fiat.setSede_legale("Otranto");
	    try{
	    	CasaProduttriceHandler.updateTuple(fiat, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(fiat.equals(found));
	    assert(found.getSede_legale().equals("Otranto"));
	}
	
	@Test
	public void stage08_SuccesfullUpdateTupleModifyingMeccanico() {
		CasaProduttrice fiat = null;
		try {
			fiat = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Set<Meccanico> mecSet = fiat.getMeccanicoSet();
		for (Meccanico m : mecSet) {
			m.setCitta(m.getCitta() + "!");
		}
		fiat.setMeccanicoSet(mecSet);
	    try{
	    	CasaProduttriceHandler.updateTuple(fiat, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(fiat.equals(found));
	}
	
	@Test
	public void stage09_SuccesfullUpdateTupleCreateNewMeccanico() {
		CasaProduttrice fiat = null;
		try {
			fiat = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Set<Meccanico> mecSet = fiat.getMeccanicoSet();
		Meccanico antonio = null;
		try {
			antonio = new Meccanico("33333333333", "Antonio", "Avellino", "Vicolo corto", "3254512548");
			utilizedMeccanico.add(antonio);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		mecSet.add(antonio);
		fiat.setMeccanicoSet(mecSet);
	    try{
	    	CasaProduttriceHandler.updateTuple(fiat, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = CasaProduttriceHandler.findTuple("Fiat", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(fiat.equals(found));
	}
	
	@Test
	public void stage10_SuccesfullDeleteTuple() {
	    try {
	    	CasaProduttriceHandler.deleteTuple("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // verifying that tuple does not exits anymore
	    try {
	    	CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    	fail(); // this is executed only if it finds a tuple which is not suppose to be there
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // Checking if mechanicals are still in the DB
	    try {
	    	MeccanicoHandler.findTuple("11001100110", entitymanager);
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    return;
	}
	
	@Test
	public void stage11_FailFindTuple() {
		try {
			CasaProduttriceHandler.findTuple("Ferrari", entitymanager);
		}	    
		catch(TupleNotFoundException e) {
	    	return;
	    }
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage12_FailAddTuple() {
		CasaProduttrice nissan2 = null;
		try {
			nissan2 = new CasaProduttrice("Nissan", "Burundi", "Hamed", null);
			utilizedCasa.add(nissan2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			CasaProduttriceHandler.addTuple(nissan2, entitymanager);
		}
		catch(EntityExistsException e) {
		   	return;
		}
		catch(Exception e) {
		   	fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage13_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		CasaProduttrice subarubaracca = null;
		try {
			subarubaracca = new CasaProduttrice("Subarubaracca", "Zimbawe", "Aiub", null);
			utilizedCasa.add(subarubaracca);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			CasaProduttriceHandler.updateTuple(subarubaracca, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage14_FailDeleteTuple() {
		try {
			CasaProduttriceHandler.deleteTuple("Subarubaracca", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}	
}