package handlerTests;

import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Auto;
import entities.CasaProduttrice;
import entities.Guidatore;
import exceptions.TupleNotFoundException;
import handlers.AutoHandler;
import handlers.CasaProduttriceHandler;
import handlers.GuidatoreHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AutoTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Auto> utilizedAuto;
	private static Set<CasaProduttrice> utilizedCaseProduttrici;
	private static Set<Guidatore> utilizedGuidatori;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedCaseProduttrici = new HashSet<CasaProduttrice>();
		utilizedGuidatori = new HashSet<Guidatore>();
		utilizedAuto = new HashSet<Auto>();
	}
	
	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Auto auto : utilizedAuto) {
			try {
				AutoHandler.deleteTuple(auto.getTarga(), entitymanager);
			} catch(Exception e) {}
		}
		for (Guidatore guidatore : utilizedGuidatori) {
			try {
				GuidatoreHandler.deleteTuple(guidatore.getCF(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice cp : utilizedCaseProduttrici) {
			try {
				CasaProduttriceHandler.deleteTuple(cp.getNome(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
		emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullAddTupleAndFindTupleNull() {
		Auto a = null;
		try {
			a = new Auto("AA000AA", null, "Jaguar", (short) 42, 10500, 22000, null);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		AutoHandler.addTuple(a, entitymanager);
		
		Auto found = null;
		Auto found2 = null;
		try {
			found = AutoHandler.findTuple("AA000AA", entitymanager);
			found2 = AutoHandler.findTuple("AA000AA", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
		// We are checking that the same object is not loaded twice
	    assert(found == found2); 
	}
	
	@Test
	public void stage02_SuccesfullUpdateTupleNull() {
		Auto a = null;
		try {
			a = AutoHandler.findTuple("AA000AA", entitymanager);
		}
		catch(Exception e) {
	    	fail(e.getMessage());
	    }
		a.setKm(40000);
		
		try {
			AutoHandler.updateTuple(a, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto found = null;
		try {
			found = AutoHandler.findTuple("AA000AA", entitymanager);
		}
		catch(Exception e) {
	    	fail(e.getMessage());
		}
		assert(a.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullDeleteTupleNull() {
		try {
			AutoHandler.deleteTuple("AA000AA", entitymanager);
		}
		catch(Exception e) {
	    	fail(e.getMessage());
		}
		try {
			AutoHandler.findTuple("AA000AA", entitymanager);
		}
		catch(TupleNotFoundException e) {
	    	return;
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		fail(); // this is executed only if it finds a tuple which is not suppose to be there
	}
	
	@Test
	public void stage04_SuccesfullAddTupleAndFindTuple() {
		Auto a = null;
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttrice", "Trento", "Francesco De Franceschi", null);
			utilizedCaseProduttrici.add(cp);
			g = new Guidatore("WWWWWW987WWW111R", "Fabio", "De Fabi", "20/01/1985", "1c", "Catanzaro", null);
			utilizedGuidatori.add(g);
			a = new Auto("BB000BB", cp, "500", (short) 21, 5600, 145095, g);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		AutoHandler.addTuple(a, entitymanager);
		
		Auto found = null;
		Auto found2 = null;
		try {
			found = AutoHandler.findTuple("BB000BB", entitymanager);
			found2 = AutoHandler.findTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
		assert(found == found2);
	    // We also check if g and cp are in DB, we do not control if we search them twice the object 
	    // is loaded only once; this is tested in their tests.
		Guidatore gFound = null;
		try {
			gFound = GuidatoreHandler.findTuple("WWWWWW987WWW111R", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.equals(gFound));
		CasaProduttrice cpFound = null;
		try {
		   cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttrice", entitymanager);
		}
		catch(Exception e) {
		   	fail(e.getMessage());
		}
		assert(cp.equals(cpFound));
	}
	
	@Test
	public void stage05_SuccesfullAddTupleAlreadyExistingExternalKey() {
		Auto a = null;
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova", "Varese", "Alessandro de Alessandri", null);
			utilizedCaseProduttrici.add(cp);
			// In this way we test both the case of a not already existing external key and the 
			// already existing external key
			g = new Guidatore("WWWWWW987WWW111R", "Fabio", "De Fabi", "20/01/1985", "1c", "Catanzaro", null);
			utilizedGuidatori.add(g);
			a = new Auto("CC000CC", cp, "208", (short) 12, 5000, 156000, g);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		AutoHandler.addTuple(a, entitymanager);
		
		Auto found = null;
		Auto found2 = null;
		try {
			found = AutoHandler.findTuple("CC000CC", entitymanager);
			found2 = AutoHandler.findTuple("CC000CC", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
		assert(found == found2);
		// We are testing if the new CasaProduttrice is in the db
	    CasaProduttrice cpFound = null;
	    try {
	    	cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(cp.equals(cpFound));
	}
	
	@Test
	public void stage06_SuccesfullUpdateTupleNotExternalKey() {
		Auto a = null;
		try {
			a = AutoHandler.findTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		a.setKm(20000);
		try {
			AutoHandler.updateTuple(a, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto found = null;
		try {
			found = AutoHandler.findTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
	}
	
	@Test
	public void stage07_SuccesfullUpdateTupleExternalKeyNewFathers() {
		Auto a = null;
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova2", "Firenze", "Marco de Marchi", null);
			utilizedCaseProduttrici.add(cp);
			g = new Guidatore("00000AAAAA999000", "Alessio", "Verdi", "13/12/1998", "5a", "Pisa", null);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			a = AutoHandler.findTuple("CC000CC", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		a.setMarca(cp);
		a.setGuidatore(g);
		
		try {
			AutoHandler.updateTuple(a, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		
		Auto found = null;
		try {
			found = AutoHandler.findTuple("CC000CC", entitymanager);
		}
		catch(Exception e){
			fail(e.getMessage());
		}
		assert(a.equals(found));
		
		// We also check if g and cp are in DB, we do not control if we search them twice the object 
	    // is loaded only once; this is tested in their tests. 
		Guidatore gFound = null;
		try {
			gFound = GuidatoreHandler.findTuple("00000AAAAA999000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.equals(gFound));
		
		CasaProduttrice cpFound = null;
	    try {
	    	cpFound = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova2", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(cp.equals(cpFound));
	}
	
	@Test
	public void stage08_SuccesfullUpdateTupleExternalKeyAlreadyExistingFather() {
		Auto a = null;
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			cp = CasaProduttriceHandler.findTuple("TestCasaProduttriceNuova", entitymanager);
			g = GuidatoreHandler.findTuple("WWWWWW987WWW111R", entitymanager);
			a = AutoHandler.findTuple("CC000CC", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		a.setMarca(cp);
		a.setGuidatore(g);
		try {
			AutoHandler.updateTuple(a, entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Auto found = null;
		try {
			found = AutoHandler.findTuple("CC000CC", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(a.equals(found));
	}
	
	@Test
	public void stage09_SuccesfullDeleteTuple() {
		try {
			AutoHandler.deleteTuple("BB000BB", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		// Verifying that the tuple does not exist anymore
		try {
			AutoHandler.findTuple("BB000BB", entitymanager);
			fail();
		}
		catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    // Checking if the fathers are still in the DB
		try {
			GuidatoreHandler.findTuple("WWWWWW987WWW111R", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			CasaProduttriceHandler.findTuple("TestCasaProduttrice", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage10_FailFindTuple() {
		try {
			AutoHandler.findTuple("ZZ000ZZ", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage11_FailAddTuple() {
		Auto a = null;
		CasaProduttrice cp = null;
		Guidatore g = null;
		try {
			cp = new CasaProduttrice("TestCasaProduttriceNuova3", "Ancona", "Giuseppe Verdi", null);
			utilizedCaseProduttrici.add(cp);
			g = new Guidatore("UUUUUU5556789123", "Filippo", "de Filippi", "15/11/2000", "10", "Sirolo", null);
			utilizedGuidatori.add(g);
			a = new Auto("CC000CC", cp, "Giulietta", (short) 15, 20000, 1500, g);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			AutoHandler.addTuple(a, entitymanager);
		}
		catch(EntityExistsException e) {
		   	return;
		}
		catch(Exception e) {
		   	fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage12_FailUpdateTuple() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		Auto a = null;
		try {
			a = new Auto("WW000WW", null, "Delta", (short) 12, 15000, 30000, null);
			utilizedAuto.add(a);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			AutoHandler.updateTuple(a, entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage13_FailDeleteTuple() {
		try {
			AutoHandler.deleteTuple("PP000PP", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();		
	}
}