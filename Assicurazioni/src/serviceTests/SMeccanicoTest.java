package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Meccanico;
import exceptions.TupleNotFoundException;
import handlers.MeccanicoHandler;
import services.ServiceMeccanico;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SMeccanicoTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Meccanico> utilizedMccanico;
	
	@BeforeClass
	public static void setUpConnection() {
		utilizedMccanico = new HashSet<Meccanico>();
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
	}
	
	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Meccanico m : utilizedMccanico) {
			try {
				MeccanicoHandler.deleteTuple(m.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullCreateMeccanicoAndFindMeccanico() {
		Meccanico m = null;
		try {
			m = ServiceMeccanico.createMeccanico("12457845121", "Marco", "Bergamo", "Via Angelino", "3332548562", entitymanager);
			utilizedMccanico.add(m);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if (m == null) 
			fail();
		Meccanico found = null;
		try {
			found = ServiceMeccanico.findMeccanico("12457845121", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(m.equals(found));
	}
	
	@Test
	public void stage02_SuccesfullUpdateMeccanico() {
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("NoMe", "Test Nome Updated");
		d.put("viA", "Test Nome Updated");
		d.put("Not a key", "Not a value");
		try {
			assert(ServiceMeccanico.updateMeccanico("12457845121", d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Meccanico m = null;
		try {
			m = ServiceMeccanico.findMeccanico("12457845121", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(m.getNome().equals("Test Nome Updated"));
		assert(m.getVia().equals("Test Nome Updated"));
	}

	@Test
	public void stage03_SuccesfullDeleteMeccanico() {
		assert(ServiceMeccanico.deleteMeccanico("12457845121", entitymanager));
		try {
			ServiceMeccanico.findMeccanico("12457845121", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void stage04_FailCreateMeccanico() {
		Meccanico m1 = null;
		Meccanico m2 = null;
		try {
			m1 = ServiceMeccanico.createMeccanico("55522288866", "Yuri", "Arezzo", "Via Angelino", "3332548562", entitymanager);
			utilizedMccanico.add(m1);
			m2 = ServiceMeccanico.createMeccanico("55522288866", "Yuri", "Arezzo", "Via Angelino", "3332548562", entitymanager);
			if(m2 != null) {
				utilizedMccanico.add(m2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage05_FailUpdateMeccanico() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		try {
			assertFalse(ServiceMeccanico.updateMeccanico("NotKeyAdded", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage06_FailDeleteMeccanico() {
		assertFalse(ServiceMeccanico.deleteMeccanico("NotKeyAdded", entitymanager));
	}
	
	public void stage07_SuccesfullSearchByCitta() {
		Meccanico m1 = null;
		Meccanico m2 = null;
		Meccanico m3 = null;
		try {
			m1 = ServiceMeccanico.createMeccanico("12457845121", "Marco", "Bergamo", "Via Angelino", "3332548562", entitymanager);
			utilizedMccanico.add(m1);
			m2 = ServiceMeccanico.createMeccanico("12457845122", "Fabio", "Bergamo", "Via degli Abruzzi", "3332544562", entitymanager);
			utilizedMccanico.add(m2);
			m3 = ServiceMeccanico.createMeccanico("12457845123", "Lugi", "Milano", "Via Washington", "3332548566", entitymanager);
			utilizedMccanico.add(m3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Meccanico> r = ServiceMeccanico.searchByCitta("Bergamo", entitymanager);
		assert(r.contains(m1));
		assert(r.contains(m2));
		assertFalse(r.contains(m3));
	}
	
}
