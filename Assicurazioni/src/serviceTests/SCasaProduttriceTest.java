package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.indirection.IndirectSet;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.CasaProduttrice;
import exceptions.TupleNotFoundException;
import handlers.CasaProduttriceHandler;
import services.ServiceCasaProduttrice;
import entities.Meccanico;
import handlers.MeccanicoHandler;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SCasaProduttriceTest {
	
	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Meccanico> utilizedMeccanico;
	private static Set<CasaProduttrice> utilizedCasa;
	
	@BeforeClass
	public static void setUpConnection() {
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
		utilizedMeccanico = new HashSet<Meccanico>();
		utilizedCasa = new HashSet<CasaProduttrice>();
	}

	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Meccanico m : utilizedMeccanico) {
			try {
				MeccanicoHandler.deleteTuple(m.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		for (CasaProduttrice c : utilizedCasa) {
			try {
				CasaProduttriceHandler.deleteTuple(c.getNome(), entitymanager);
			} catch(Exception e) {}
		}
    	entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullCreateCasaProduttriceNoMeccaniciAndFind() {
		CasaProduttrice fiat = null;
		try {
			fiat = ServiceCasaProduttrice.createCasaProduttriceWithoutMeccanici("Fiat", "Italia", "Anastasio", entitymanager);
		    utilizedCasa.add(fiat);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(fiat == null) fail();
	    
	    CasaProduttrice found = null;
	    try{
	    	found = ServiceCasaProduttrice.findCasaProduttrice("Fiat", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(fiat.equals(found));
	}
	
	@Test
	public void stage02_SuccesfullCreateCasaProduttriceNewMeccaniciAndFind() {
		Meccanico luca = null;
		Meccanico mario = null;
		CasaProduttrice nissan = null;
		Set<Meccanico> mecSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  mario = new Meccanico("11001100110", "Mario", "Milano", "Via angelo", "3334564432");
	    	  
	    	  utilizedMeccanico.add(mario);
	    	  mecSet = new IndirectSet();
	    	  mecSet.add(luca);
	    	  mecSet.add(mario);
	    	  nissan = ServiceCasaProduttrice.createCasaProduttriceWithMeccanici("Nissan", "Libano", "Abdoul", mecSet, entitymanager);
	    	  utilizedCasa.add(nissan);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    CasaProduttrice found = null;
	    try{
	    	found = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(nissan.equals(found));	    
	    try {
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    	MeccanicoHandler.findTuple("11001100110", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
		
	}
	
	@Test
	public void stage03_SuccesfullAddTupleAlreadyExistingMeccanico() {
		Meccanico luca = null;
		Meccanico mario = null;
		CasaProduttrice alfa = null;
		Set<Meccanico> mecSet = null;
		
		try {
	    	  luca = new Meccanico("22222222222", "Luca", "Roma", "Piazza del popolo", "3338432123");
	    	  utilizedMeccanico.add(luca);
	    	  mario = new Meccanico("11001100110", "Mario", "Milano", "Via angelo", "3334564432");
	    	  utilizedMeccanico.add(mario);
	    	  mecSet = new IndirectSet();
	    	  mecSet.add(luca);
	    	  mecSet.add(mario);
	    	  alfa = ServiceCasaProduttrice.createCasaProduttriceWithMeccanici("Alfa", "Roma", "Luigimo", mecSet, entitymanager);
	    	  utilizedCasa.add(alfa);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}	    
	    CasaProduttrice found = null;
	    try{
	    	found = ServiceCasaProduttrice.findCasaProduttrice("Alfa", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    assert(alfa.equals(found));
	    
	}
	
	@Test
	public void stage04_SuccesfullUpdateTupleNotInvolvingMeccanico() {
		CasaProduttrice nissan = null;
		try {
			nissan = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("reFerEnte", "Updated Referente");
		d.put("SEDE_lEgale", "Updated Sede Legale");
	    try{
	    	ServiceCasaProduttrice.updateCasaProduttrice("Nissan", d, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(nissan.equals(found));
	    assert(found.getSede_legale().equals("Updated Sede Legale"));
	}
	
	@Test
	public void stage05_SuccesfullUpdateTupleReplacingMeccanico() {
		CasaProduttrice nissan = null;
		try {
			nissan = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("reFerEnte", "Updated Referente");
		d.put("SEDE_lEgale", "Updated Sede Legale");
		Set<Meccanico> mecSet = nissan.getMeccanicoSet();
		for (Meccanico m : mecSet) {
			m.setCitta(m.getCitta() + "!");
		}
	    try{
	    	ServiceCasaProduttrice.updateCasaProduttrice("Nissan", mecSet, d, true ,entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(nissan.equals(found));
	    assert(found.getSede_legale().equals("Updated Sede Legale"));
	}
	
	@Test
	public void stage06_SuccesfullUpdateTupleAddingMeccanico() {
		CasaProduttrice nissan = null;
		try {
			nissan = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("reFerEnte", "Updated Referente");
		d.put("SEDE_lEgale", "Updated Sede Legale");
		Set<Meccanico> mecSet = new IndirectSet();
		Meccanico luchino = null;
		try {
			luchino = new Meccanico("12548569857", "Luchino", "Liverpool", "street stretta", "879542548");
			utilizedMeccanico.add(luchino);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		mecSet.add(luchino);
	    try{
	    	ServiceCasaProduttrice.updateCasaProduttrice("Nissan", mecSet, d, false, entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    CasaProduttrice found = null;
	    try {
			found = ServiceCasaProduttrice.findCasaProduttrice("Nissan", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	    assert(nissan.equals(found));
	    assert(found.getSede_legale().equals("Updated Sede Legale"));
	    try {
	    	MeccanicoHandler.findTuple("12548569857", entitymanager);
	    } 
	    catch (TupleNotFoundException e)
	    {
	    	fail(e.getMessage());
	    }
	}
	
	@Test
	public void stage07_SuccesfullDeleteCasaProduttrice() {
		assert(ServiceCasaProduttrice.deleteCasaProduttrice("Fiat", entitymanager));
	    try {
	    	CasaProduttriceHandler.findTuple("Fiat", entitymanager);
	    	fail(); 
	    }
	    catch(TupleNotFoundException e) {
	    	// this is what we expect
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    try {
	    	MeccanicoHandler.findTuple("11001100110", entitymanager);
	    	MeccanicoHandler.findTuple("22222222222", entitymanager);
	    }
	    catch(Exception e) {
	    	fail(e.getMessage());
	    }
	    return;
	}

	@Test
	public void stage08_FailCreateCasaProduttriceDuplicateKey() {
		CasaProduttrice c1 = null;
		CasaProduttrice c2 = null;
		try {
			c1 = ServiceCasaProduttrice.createCasaProduttriceWithoutMeccanici("Fiat", "Italia", "Anastasio", entitymanager);
			utilizedCasa.add(c1);
			c2 = ServiceCasaProduttrice.createCasaProduttriceWithoutMeccanici("Fiat", "Romania", "Geco", entitymanager);
			if(c2 != null) {
				utilizedCasa.add(c2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage09_FailUpdateCasaProduttrice() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		// Because both method use the same logic to fail (they use private methods), 
		// testing one of them is enough
		try {
			assertFalse(ServiceCasaProduttrice.updateCasaProduttrice("Subarubaracca", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}

	@Test
	public void stage10_FailDeleteCasaProduttrice() {
		assertFalse(ServiceCasaProduttrice.deleteCasaProduttrice("Subarubaracca", entitymanager));
	}
}
