package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Societa;
import exceptions.TupleNotFoundException;
import handlers.SocietaHandler;
import services.ServiceSocieta;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SSocietaTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Societa> utilizedSocieta;
	
	@BeforeClass
	public static void setUpConnection() {
		utilizedSocieta = new HashSet<Societa>();
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
	}
	
	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Societa societa : utilizedSocieta) {
			try {
				SocietaHandler.deleteTuple(societa.getPiva(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullCreateSocietaAndFindSocieta() {
		Societa s = null;
		try {
			s = ServiceSocieta.createSocieta("TestPiva000", "Test Societa", "Arezzo", "Test", entitymanager);
			utilizedSocieta.add(s);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if (s == null) fail();
		Societa found = null;
		try {
			found = ServiceSocieta.findSocieta("TestPiva000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(s.equals(found));
	}
	
	@Test
	public void stage02_SuccesfullUpdateSocieta() {
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("ragione_sociale", "Test Updated");
		d.put("NOME", "Test Societa Updated");
		d.put("Not a key", "Not a value");
		try {
			assert(ServiceSocieta.updateSocieta("TestPiva000", d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Societa s = null;
		try {
			s = ServiceSocieta.findSocieta("TestPiva000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(s.getNome().equals("Test Societa Updated"));
		assert(s.getRagione_sociale().equals("Test Updated"));
	}

	@Test
	public void stage03_SuccesfullDeleteSocieta() {
		assert(ServiceSocieta.deleteSocieta("TestPiva000", entitymanager));
		try {
			ServiceSocieta.findSocieta("TestPiva000", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void stage04_FailCreateSocieta() {
		Societa s = null;
		Societa s2 = null;
		try {
			s = ServiceSocieta.createSocieta("TestPiva001", "Test Societa1", "Arezzo", "Test1", entitymanager);
			utilizedSocieta.add(s);
			s2 = ServiceSocieta.createSocieta("TestPiva001", "Test Societa", "Arezzo", "Test2", entitymanager);
			if(s2 != null) {
				utilizedSocieta.add(s2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage05_FailUpdateSocieta() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		try {
			assertFalse(ServiceSocieta.updateSocieta("NotKeyAdded", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage06_FailDeleteSocieta() {
		assertFalse(ServiceSocieta.deleteSocieta("NotKeyAdded", entitymanager));
	}

	@Test
	public void stage07_SuccesfullSearchBySede() {
		Societa s1 = null;
		Societa s2 = null;
		Societa s3 = null;
		try {
			s1 = ServiceSocieta.createSocieta("TestPiva100", "Test Societa", "Bari", "Test", entitymanager);
			utilizedSocieta.add(s1);
			s2 = ServiceSocieta.createSocieta("TestPiva200", "Test Societa", "Bari", "Test", entitymanager);
			utilizedSocieta.add(s2);
			s3 = ServiceSocieta.createSocieta("TestPiva300", "Test Societa", "Torino", "Test", entitymanager);
			utilizedSocieta.add(s3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}	
		List<Societa> r = ServiceSocieta.searchBySede("Bari", entitymanager);
		assert(r.contains(s1));
		assert(r.contains(s2));
		assertFalse(r.contains(s3));
	}
	
}