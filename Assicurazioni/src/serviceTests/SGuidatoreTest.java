package serviceTests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import entities.Guidatore;
import exceptions.TupleNotFoundException;
import handlers.GuidatoreHandler;
import services.ServiceGuidatore;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SGuidatoreTest {

	private static EntityManagerFactory emfactory;
	private static EntityManager entitymanager;
	private static Set<Guidatore> utilizedGuidatori;
	
	@BeforeClass
	public static void setUpConnection() {
		utilizedGuidatori = new HashSet<Guidatore>();
		emfactory = Persistence.createEntityManagerFactory("Assicurazioni");
		entitymanager = emfactory.createEntityManager();
	}
	
	@AfterClass
	public static void cleanUpDbAndDestroyConnection() {
		for (Guidatore guidatore : utilizedGuidatori) {
			try{
				GuidatoreHandler.deleteTuple(guidatore.getCF(), entitymanager);
			} catch(Exception e) {}
		}
		entitymanager.close();
    	emfactory.close();
	}
	
	@Test
	public void stage01_SuccesfullCreateGuidatoreNoGenitoreAndFind() {
		Guidatore g = null;
		try {
			g = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale000", "Mario", "Rossi", "13/12/1999", "3a", "Roma", entitymanager);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(g == null) fail();
		Guidatore found = null;
		try {
			found = ServiceGuidatore.findGuidatore("CodiceFiscale000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.equals(found));
	}

	@Test
	public void stage02_SuccesfullCreateGuidatoreExistingGenitore() {
		Guidatore g = null;
		Guidatore genitore = null;
		try {
			genitore = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale002", "Carlo", "de Carli", "02/04/1993", "2", "Firenze", entitymanager);
			utilizedGuidatori.add(genitore);
			g = ServiceGuidatore.createGuidatoreAlreadyExistingGenitore("CodiceFiscale004", 
					"Ugo", "de Ugo", "14/09/1998", "5c", "Torino", "CodiceFiscale002", entitymanager);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		if(g == null) fail();
		Guidatore found = null;
		try {
			found = ServiceGuidatore.findGuidatore("CodiceFiscale004", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.equals(found));
	}
	
	@Test
	public void stage03_SuccesfullUpdateGuidatoreNotGenitore() {
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("ciTta", "citta Updated");
		d.put("NOME", "Guidatore Updated");
		d.put("Not a key", "Not a value");
		try {
			assert(ServiceGuidatore.updateGuidatore("CodiceFiscale000", d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Guidatore g = null;
		try {
			g = ServiceGuidatore.findGuidatore("CodiceFiscale000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.getCitta().equals("citta Updated"));
		assert(g.getNome().equals("Guidatore Updated"));
	}
	
	@Test
	public void stage04_SuccesfullUpdateGuidatoreGenitore() {
		HashMap<String, String> d = new HashMap<String, String>();
		d.put("COGNOME", "cognome Updated");
		Guidatore g = null;
		try{
			g = new Guidatore("CodiceFiscale005", 
				"nome", "cognome", "18/06/1967", "1a", "Roma", null);
			utilizedGuidatori.add(g);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		try {
			assert(ServiceGuidatore.updateGuidatore("CodiceFiscale000", g, d, entitymanager));
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		Guidatore found = null;
		try {
			found = ServiceGuidatore.findGuidatore("CodiceFiscale000", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(found.getCognome().equals("cognome Updated"));
		
		try {
			found = ServiceGuidatore.findGuidatore("CodiceFiscale005", entitymanager);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		assert(g.equals(found));
	}
	
	@Test
	public void stage05_SuccesfullDeleteGuidatore() {
		assert(ServiceGuidatore.deleteGuidatore("CodiceFiscale000", entitymanager));
		try {
			ServiceGuidatore.findGuidatore("CodiceFiscale000", entitymanager);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void stage06_FailCreateGuidatoreDuplicateKey() {
		Guidatore g = null;
		Guidatore g2 = null;
		try {
			g = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale010", "Fabio", 
					"de Fabi", "09/09/1998", "3c", "Milano", entitymanager);
			utilizedGuidatori.add(g);
			g2 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale010", "Luigi", 
					"de Luigi", "05/08/1994", "2", "Trento", entitymanager);
			if(g2 != null) {
				utilizedGuidatori.add(g2);
				fail();
			}
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}
	
	@Test
	public void stage07_FailCreateGuidatoreNonExistingGenitore() {
		Guidatore g = null;
		try {
			g = ServiceGuidatore.createGuidatoreAlreadyExistingGenitore("CodiceFiscale011", 
					"Gianni", "de Gianni", "03/04/1993", "4a", "Napoli", "CodiceFiscale999", entitymanager);
			utilizedGuidatori.add(g);
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		fail();
	}
	
	@Test
	public void stage08_FailUpdateGuidatore() {
		// Because modifying the primary key is not allowed (the set method is private), this method can fail only in the case
		// it does not find the tuple to modify
		// Because both method use the same logic to fail (they use private methods), 
		// testing one of them is enough
		try {
			assertFalse(ServiceGuidatore.updateGuidatore("CodiceFiscaleNOT", null, entitymanager));
		}
		catch(TupleNotFoundException e) {
			return;
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		return;
	}

	@Test
	public void stage09_FailDeleteSocieta() {
		assertFalse(ServiceGuidatore.deleteGuidatore("CodiceFiscaleNOT", entitymanager));
	}	
	
	@Test
	public void stage10_SuccesfullSearchByCitta() {
		Guidatore g1 = null;
		Guidatore g2 = null;
		Guidatore g3 = null;
		try {
			g1 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale100", "Giuseppe", "Rossi", "13/12/1974", "1", "Trieste", entitymanager);
			utilizedGuidatori.add(g1);
			g2 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale200", "Francesco", "Rossi", "13/12/1984", "1", "Trieste", entitymanager);
			utilizedGuidatori.add(g2);
			g3 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale300", "Mario", "Rossi", "13/12/1976", "3a", "Ancona", entitymanager);
			utilizedGuidatori.add(g3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Guidatore> r = ServiceGuidatore.searchByCitta("Trieste", entitymanager);
		assert(r.contains(g1));
		assert(r.contains(g2));
		assertFalse(r.contains(g3));
	}
	
	@Test
	public void stage11_SuccesfullSearchByClasse() {
		Guidatore g1 = null;
		Guidatore g2 = null;
		Guidatore g3 = null;
		try {
			g1 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale900", "Giuseppe", "Rossi", "13/12/1974", "1", "Trieste", entitymanager);
			utilizedGuidatori.add(g1);
			g2 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale800", "Francesco", "Rossi", "13/12/1984", "1", "Trieste", entitymanager);
			utilizedGuidatori.add(g2);
			g3 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale700", "Mario", "Rossi", "13/12/1976", "3a", "Ancona", entitymanager);
			utilizedGuidatori.add(g3);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Guidatore> r = ServiceGuidatore.searchByClasse("1", entitymanager);
		assert(r.contains(g1));
		assert(r.contains(g2));
		assertFalse(r.contains(g3));
	}
	
	@Test
	public void stage12_SuccesfullSearchByGenitore() {
		Guidatore g1 = null;
		Guidatore g2 = null;
		Guidatore g3 = null;
		try {
			g3 = ServiceGuidatore.createGuidatoreWithoutGenitore("CodiceFiscale600", "Giuseppe", "Rossi", "13/12/1974", "1", "Trieste", entitymanager);
			utilizedGuidatori.add(g3);
			g1 = ServiceGuidatore.createGuidatoreAlreadyExistingGenitore("CodiceFiscale500", "Dario", "Rossi", "15/06/1996", "1", "Trieste", "CodiceFiscale600", entitymanager);
			utilizedGuidatori.add(g1);
			g2 = ServiceGuidatore.createGuidatoreAlreadyExistingGenitore("CodiceFiscale400", "Riccardo", "Rossi", "21/02/1999", "1", "Trieste", "CodiceFiscale600", entitymanager);
			utilizedGuidatori.add(g2);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
		List<Guidatore> r = ServiceGuidatore.searchByGenitore("CodiceFiscale600", entitymanager);
		assert(r.contains(g1));
		assert(r.contains(g2));
		assertFalse(r.contains(g3));
	}
}