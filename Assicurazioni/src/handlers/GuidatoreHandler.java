package handlers;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Auto;
import entities.Guidatore;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class GuidatoreHandler {
	
	public static void addTuple(Guidatore guidatore, EntityManager em) {
		em.getTransaction().begin();
		try {
			findTuple(guidatore.getCF(), em);
			// if it is already in the db
			em.getTransaction().rollback();
			throw new EntityExistsException();
		}
		catch(TupleNotFoundException e) {
			// this is what we want
		}
		em.merge(guidatore);
		em.getTransaction().commit();
	}
	
	public static Guidatore findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Guidatore tmp = em.find(Guidatore.class, pKey); 
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(Guidatore guidatore, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		Guidatore tmp = null;
		try {
			tmp = findTuple(guidatore.getCF(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(guidatore);
		em.merge(tmp);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		em.getTransaction().begin();
		Guidatore tmp = null;
		try {
			tmp = findTuple(pKey, em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		Query q = em.createQuery("SELECT f FROM Guidatore f INNER JOIN f.genitore g WHERE g.CF = :cf");
		q.setParameter("cf", pKey);
		List<Guidatore> sons = (List<Guidatore>) q.getResultList();
		if(sons != null) {
			for (Guidatore guidatore : sons) {
				guidatore.setGenitore(null);
				em.merge(guidatore);
			}
		}
		q = em.createQuery("SELECT a FROM Auto a INNER JOIN a.guidatore g WHERE g.CF = :cf");
		q.setParameter("cf", pKey);
		List<Auto> autos = (List<Auto>) q.getResultList();
		if(autos != null) {
			for (Auto auto : autos) {
				auto.setGuidatore(null);
				em.merge(auto);
			}
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}
}