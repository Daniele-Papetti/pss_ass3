package handlers;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;

import entities.Bilico;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class BilicoHandler {
	
	public static void addTuple(Bilico bilico, EntityManager em) {
		em.getTransaction().begin();
		try {
			findTuple(bilico.getTarga(), em);
			// if it is already in the db
			em.getTransaction().rollback();
			throw new EntityExistsException();
		}
		catch(TupleNotFoundException e) {
			// this is what we want
		}
		em.merge(bilico);
		em.getTransaction().commit();
	}
	
	public static Bilico findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Bilico tmp = em.find(Bilico.class, pKey);
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(Bilico bilico, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		Bilico tmp = null;
		try {
			tmp = findTuple(bilico.getTarga(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(bilico);
		em.merge(tmp);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		em.getTransaction().begin();
		Bilico tmp = null;
		try {
			tmp = findTuple(pKey, em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}
}