package handlers;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Bilico;
import entities.Societa;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;

public class SocietaHandler {
	
	public static void addTuple(Societa societa, EntityManager em) {
		em.getTransaction().begin();
		try{
			em.persist(societa);
		}
		catch(EntityExistsException e) {
			em.getTransaction().rollback();
			throw e;
		}
		em.getTransaction().commit();
	}
	
	public static Societa findTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		Societa tmp = em.find(Societa.class, pKey);
		if (tmp == null)
			throw new TupleNotFoundException();
		return tmp;
	}
	
	public static void updateTuple(Societa societa, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		em.getTransaction().begin();
		Societa tmp = null;
		try {
			tmp = findTuple(societa.getPiva(), em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		tmp.setEqual(societa);
		em.getTransaction().commit();
	}
	
	public static void deleteTuple(String pKey, EntityManager em) throws TupleNotFoundException {
		em.getTransaction().begin();
		Societa tmp = null;
		try {
			tmp = findTuple(pKey, em);
		}
		catch(TupleNotFoundException e) {
			em.getTransaction().rollback();
			throw e;
		}
		Query q = em.createQuery("SELECT b FROM Bilico b INNER JOIN b.societa s WHERE s.piva = :piva");
		q.setParameter("piva", pKey);
		List<Bilico> bilici= (List<Bilico>) q.getResultList();
		if(bilici != null) {
			for (Bilico b : bilici) {
				b.setSocieta(null);
				em.merge(b);
			}
		}
		em.remove(tmp);
		em.getTransaction().commit();
	}
	
	// add some useful methods implementing some useful queries (?) i.e.: search by name

}
