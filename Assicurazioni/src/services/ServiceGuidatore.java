package services;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Guidatore;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.GuidatoreHandler;

public class ServiceGuidatore {
	
	private static Guidatore pushGuidatore(String cF, String nome, String cognome, String data_di_nascita,
			String classe, String citta, Guidatore genitore, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		Guidatore g = new Guidatore(cF, nome, cognome, data_di_nascita, classe, citta, genitore);
		try {
			GuidatoreHandler.addTuple(g, em);
		}
		catch(PersistenceException e) { 
			em.detach(g);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return g;
	}
	
	public static Guidatore createGuidatoreWithoutGenitore(String cF, String nome, String cognome, String data_di_nascita,
			String classe, String citta, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		return pushGuidatore(cF, nome, cognome, data_di_nascita, classe, citta, null, em);
	}
	
	public static Guidatore createGuidatoreAlreadyExistingGenitore(String cF, String nome, String cognome, String data_di_nascita,
			String classe, String citta, String gCF, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
			Guidatore genitore = GuidatoreHandler.findTuple(gCF, em);
			return pushGuidatore(cF, nome, cognome, data_di_nascita, classe, citta, genitore, em);
	}
	
	public static Guidatore findGuidatore(String cF, EntityManager em) throws TupleNotFoundException {
		return GuidatoreHandler.findTuple(cF, em);
	}
	
	private static Guidatore updateMap(String cF, HashMap<String, String> d, EntityManager em) 
			throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Guidatore g = new Guidatore(GuidatoreHandler.findTuple(cF, em));
		if(d == null) return g;
		if(d.isEmpty()) return g;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("nome".equalsIgnoreCase(key)) {
				g.setNome(d.get(key));
			}
			else if("cognome".equalsIgnoreCase(key)) {
				g.setCognome(d.get(key));
			}
			else if("data_di_nascita".equalsIgnoreCase(key)) {
				g.setData_di_nascita(d.get(key));
			}
			else if("classe".equalsIgnoreCase(key)) {
				g.setClasse(d.get(key));
			}
			else if("citta".equalsIgnoreCase(key)) {
				g.setCitta(d.get(key));
			}
		}
		return g;
	}
	
	private static boolean pushUpdated(Guidatore g, EntityManager em) {
		try {
			GuidatoreHandler.updateTuple(g, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public static boolean updateGuidatore(String cF, HashMap<String, String> d, EntityManager em) 
			throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Guidatore g = updateMap(cF, d, em);
		return pushUpdated(g, em);
	}
	
	public static boolean updateGuidatore(String cF, Guidatore genitore, HashMap<String, String> d, EntityManager em) 
			throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Guidatore g = updateMap(cF, d, em);
		g.setGenitore(genitore);
		return pushUpdated(g, em);
	}
	
	public static boolean deleteGuidatore(String cF, EntityManager em) {
		try {
			GuidatoreHandler.deleteTuple(cF, em);
		}
		catch(TupleNotFoundException e){
			return false;
		}
		return true;
	}
	
	public static List<Guidatore> searchByCitta(String citta, EntityManager em) {
		Query q = em.createQuery("SELECT g FROM Guidatore g WHERE g.citta = :citta");
		q.setParameter("citta", citta);
		return (List<Guidatore>) q.getResultList();
	}
	
	public static List<Guidatore> searchByClasse(String classe, EntityManager em) {
		Query q = em.createQuery("SELECT g FROM Guidatore g WHERE g.classe = :classe");
		q.setParameter("classe", classe);
		return (List<Guidatore>) q.getResultList();		
	}
	
	public static List<Guidatore> searchByGenitore(String cf, EntityManager em) {
		Query q = em.createQuery("SELECT f FROM Guidatore f INNER JOIN f.genitore g WHERE g.CF = :cf");
		q.setParameter("cf", cf);
		return (List<Guidatore>) q.getResultList();
	}
	
}
