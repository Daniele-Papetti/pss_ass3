package services;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Societa;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.SocietaHandler;

public class ServiceSocieta {

	public static Societa createSocieta(String piva, String nome, String sede_legale, String ragione_sociale, 
			EntityManager em) throws NotValidPrimaryKeyExcpetion {
		Societa s = new Societa(piva, nome, sede_legale, ragione_sociale);
		// We propagate the Exception in order to let the user know that the parameters to build the 
		// object are not valid
		try {
			SocietaHandler.addTuple(s, em);
		}
		catch(PersistenceException e) { 
			em.detach(s);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return s;
	}
	
	public static Societa findSocieta(String piva, EntityManager em) throws TupleNotFoundException {
		return SocietaHandler.findTuple(piva, em);
	}
	
	public static boolean updateSocieta(String piva, HashMap<String, String> d, EntityManager em) 
			throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		Societa s = new Societa(SocietaHandler.findTuple(piva, em));
		// We let the user know the entity is not in the db
		if(d == null) return true;
		if(d.isEmpty()) return true;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("nome".equalsIgnoreCase(key)) {
				s.setNome(d.get(key));
			}
			else if("sede_legale".equalsIgnoreCase(key)) {
				s.setSede_legale(d.get(key));
			}
			else if("ragione_sociale".equalsIgnoreCase(key)) {
				s.setRagione_sociale(d.get(key));
			}
		}
		try {
			SocietaHandler.updateTuple(s, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public static boolean deleteSocieta(String piva, EntityManager em) {
		try {
			SocietaHandler.deleteTuple(piva, em);
		}
		catch(TupleNotFoundException e){
			return false;
		}
		return true;
	}
	
	public static List<Societa> searchBySede(String sede, EntityManager em) {
		Query q = em.createQuery("SELECT s FROM Societa s WHERE s.sede_legale = :sede");
		q.setParameter("sede", sede);
		return (List<Societa>) q.getResultList();
	}
}