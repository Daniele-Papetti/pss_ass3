package services;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import entities.Auto;
import entities.CasaProduttrice;
import entities.Guidatore;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.AutoHandler;
import handlers.GuidatoreHandler;

public class ServiceAuto {
	
	private static Auto pushAuto(String targa, CasaProduttrice cp, String modello, short cilindrata,
			int valore, int km, Guidatore g, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		Auto a = new Auto(targa, cp, modello, cilindrata, valore, km, g);
		try {
			AutoHandler.addTuple(a, em);
		}
		catch(PersistenceException e) { 
			em.detach(a);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return a;
	}
	
	public static Auto createAutoAndGuidatore(String targa, CasaProduttrice cp, String modello, short cilindrata,
			int valore, int km, String gCF, String gNome, String gCognome, String gData_di_nascita,
			String gClasse, String gCitta, String CFGenitore, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		Guidatore g = null;
		if(!(CFGenitore.equals(""))) {
			try {
				g = ServiceGuidatore.createGuidatoreAlreadyExistingGenitore(gCF, gNome, gCognome, gData_di_nascita, 
						gClasse, gCitta, CFGenitore, em);
			}
			catch(NotValidPrimaryKeyExcpetion e) {
				throw new NotValidPrimaryKeyExcpetion("CF not valid!");
			}
		}
		else {
			try {
				g = ServiceGuidatore.createGuidatoreWithoutGenitore(gCF, gNome, gCognome, gData_di_nascita, gClasse, gCitta, em);
			}
			catch(NotValidPrimaryKeyExcpetion e) {
				throw new NotValidPrimaryKeyExcpetion("CF not valid!");
			}
		}
		if (g == null) throw new EntityExistsException("This guidatore already exists!");
		return pushAuto(targa, cp, modello, cilindrata, valore, km, g, em);
	}
	
	public static Auto createAutoAlreadyExistingGuidatore(String targa, CasaProduttrice cp, 
			String modello, short cilindrata, int valore, int km, String gCF, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		Guidatore g = GuidatoreHandler.findTuple(gCF, em);
		return pushAuto(targa, cp, modello, cilindrata, valore, km, g, em);
	}
	
	public static Auto findAuto(String targa, EntityManager em) throws TupleNotFoundException {
		return AutoHandler.findTuple(targa, em);
	}
	
	private static Auto updateAttributes(String targa, String modello, HashMap<String, Number> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Auto a = new Auto(AutoHandler.findTuple(targa, em));
		if(modello != "") a.setModello(modello);
		if(d == null) return a;
		if(d.isEmpty()) return a;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("cilindrata".equalsIgnoreCase(key)) {
				a.setCilindrata((short) d.get(key));
			}
			else if("valore".equalsIgnoreCase(key)) {
				a.setValore((int) d.get(key));
			}
			else if("km".equalsIgnoreCase(key)) {
				a.setKm((int) d.get(key));
			}
		}
		return a;
	}
	
	private static boolean pushUpdated(Auto a, EntityManager em) {
		try {
			AutoHandler.updateTuple(a, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}

	public static boolean updateAuto(String targa, String modello, 
			HashMap<String, Number> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Auto a = updateAttributes(targa, modello, d, em);
		return pushUpdated(a, em);
	}

	public static boolean updateAuto(String targa, String modello, CasaProduttrice cp, Guidatore g,
			HashMap<String, Number> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Auto a = updateAttributes(targa, modello, d, em);
		a.setMarca(cp);
		a.setGuidatore(g);
		return pushUpdated(a, em);
	}

	public static boolean updateAuto(String targa, String modello, CasaProduttrice cp,
			HashMap<String, Number> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Auto a = updateAttributes(targa, modello, d, em);
		a.setMarca(cp);
		return pushUpdated(a, em);
	}

	public static boolean updateAuto(String targa, String modello, Guidatore g,
			HashMap<String, Number> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		Auto a = updateAttributes(targa, modello, d, em);
		a.setGuidatore(g);
		return pushUpdated(a, em);
	}
	
	public static boolean deleteAuto(String targa, EntityManager em) {
		try {
			AutoHandler.deleteTuple(targa, em);
		}
		catch(TupleNotFoundException e) {
			return false;
		}
		return true;
	}
	
	public static List<Auto> searchByModello(String modello, EntityManager em) {
		Query q = em.createQuery("SELECT a FROM Auto a WHERE a.modello = :modello");
		q.setParameter("modello", modello);
		return (List<Auto>) q.getResultList();
	}
	
	public static List<Auto> searchByMarca(String marca, EntityManager em) {
		Query q = em.createQuery("SELECT a FROM Auto a INNER JOIN a.marca m WHERE m.nome = :marca");
		q.setParameter("marca", marca);
		return (List<Auto>) q.getResultList();
	}
	
	public static List<Auto> searchByGuidatore(String cf, EntityManager em) {
		Query q = em.createQuery("SELECT a FROM Auto a INNER JOIN a.guidatore g WHERE g.CF = :cf");
		q.setParameter("cf", cf);
		return (List<Auto>) q.getResultList();
	}
	
}
