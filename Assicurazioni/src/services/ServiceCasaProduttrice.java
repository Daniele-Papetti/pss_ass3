package services;
import java.util.HashMap;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import entities.CasaProduttrice;
import entities.Meccanico;
import exceptions.NotValidPrimaryKeyExcpetion;
import exceptions.TupleNotFoundException;
import handlers.CasaProduttriceHandler;

public class ServiceCasaProduttrice {

	private static CasaProduttrice pushCasaProduttrice(String nome, String sede_legale, String referente, Set<Meccanico> meccanicoSet, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		CasaProduttrice c = new CasaProduttrice(nome, sede_legale, referente, meccanicoSet);
		try {
			CasaProduttriceHandler.addTuple(c, em);
		}
		catch(PersistenceException e) { 
			em.detach(c);
			return null; // in this way we also communicate that the push on Db failed 
		}
		return c;
	}
	
	public static CasaProduttrice createCasaProduttriceWithoutMeccanici(String nome, String sede_legale, String referente, EntityManager em) throws NotValidPrimaryKeyExcpetion {
		return pushCasaProduttrice(nome, sede_legale, referente, null, em);
	}
	
	public static CasaProduttrice createCasaProduttriceWithMeccanici(String nome, String sede_legale, String referente, Set<Meccanico> meccanicoSet, EntityManager em) throws TupleNotFoundException, NotValidPrimaryKeyExcpetion {
		return pushCasaProduttrice(nome, sede_legale, referente, meccanicoSet, em);
	}
	
	
	private static CasaProduttrice updateMap(String nome, HashMap<String, String> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		CasaProduttrice c = new CasaProduttrice(CasaProduttriceHandler.findTuple(nome, em));
		if(d == null) return c;
		if(d.isEmpty()) return c;
		Set<String> keys = d.keySet();
		for (String key : keys) {
			if("sede_legale".equalsIgnoreCase(key)) {
				c.setSede_legale(d.get(key));
			}
			else if("referente".equalsIgnoreCase(key)) {
				c.setReferente(d.get(key));
			}
		}
		return c;
	}
	
	private static boolean pushUpdated(CasaProduttrice c, EntityManager em) {
		try {
			CasaProduttriceHandler.updateTuple(c, em);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public static boolean updateCasaProduttrice(String nome, HashMap<String, String> d, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		CasaProduttrice c = updateMap(nome, d, em);
		return pushUpdated(c, em);
	}
	
	public static boolean updateCasaProduttrice(String nome, Set<Meccanico> mecSet, HashMap<String, String> d, boolean replace, EntityManager em) throws NotValidPrimaryKeyExcpetion, TupleNotFoundException {
		CasaProduttrice c = updateMap(nome, d, em);
		Set<Meccanico> localSet = null;
		if (replace)
			c.setMeccanicoSet(mecSet);
		else
		{
			localSet = c.getMeccanicoSet();
			for (Meccanico m : mecSet) {
				localSet.add(m);
			}
			c.setMeccanicoSet(localSet);
		}
		return pushUpdated(c, em);
	}
	
	public static CasaProduttrice findCasaProduttrice(String nome, EntityManager em) throws TupleNotFoundException {
		return CasaProduttriceHandler.findTuple(nome, em);
	}
	
	public static boolean deleteCasaProduttrice(String nome, EntityManager em) {
		try {
			CasaProduttriceHandler.deleteTuple(nome, em);
		}
		catch(TupleNotFoundException e){
			return false;
		}
		return true;
	}
}
